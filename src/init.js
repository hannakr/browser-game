import { createKey } from './utils';

function draw_tile(canvas, img, ground) {
  //const canvas = document.getElementById('cw');
  if (!canvas.getContext) {
    return;
  }
  const ctx = canvas.getContext('2d');

  //const imgwidth = 45;
  const imgwidth = 80;
  //const imgheight = 45;
  const imgheight = 80;
  const scalex = 0.25;
  const scaley = 0.25;

  const tilesetNumTiles = 3;  //number of tiles per row on the tileset
  const tileSize = 80;        //the size of the tile (unscaled)
  const rowTileCount = 25;    //number of tiles in a row of the background
  const colTileCount = 25;    //number of tiles in a column of the background

  const grass1Level1 = 0;
  const grass2Level1 = 1;
  const grass1Level2 = 2;
  const grass2Level2 = 3;
  const deadGrass = 4;
  const dirt = 5;
  const seeds = 6;
  const soil = 8;
  const selectDirt = 9;


  const tiles = {
    'grass1Level1': {'x': 0, 'y': 0},
    'grass2Level1': {'x': imgwidth, 'y': 0},
    'grass1Level2': {'x': 2.0*imgwidth, 'y': 0},
    'grass2Level2': {'x': 0, 'y': imgheight},
    'deadGrass': {'x': imgwidth, 'y': imgheight},
    'dirt': {'x': 2.0*imgwidth, 'y': imgheight}
  }


  //ctx.drawImage(img, dirtx, dirty, imgwidth, imgheight, 0, 0, scalex*imgwidth, scaley*imgheight);
  //ctx.drawImage(img, dirtx, dirty, imgwidth, imgheight, scalex*imgwidth, 0, scalex*imgwidth, scaley*imgheight);
  for (let r = 0; r < rowTileCount; r++) {
      for (let c = 0; c < colTileCount; c++) {
         const tile = ground[ r ][ c ];
         const tileRow = (tile / tilesetNumTiles) | 0; // Bitwise OR operation
         const tileCol = (tile % tilesetNumTiles) | 0;
         ctx.drawImage(img, (tileCol * tileSize), (tileRow * tileSize), tileSize, tileSize, (c * scalex * tileSize), (r * scaley * tileSize), scalex*tileSize, scaley*tileSize);
      }
   }

}

export function init (canvas, state) {
  const { tiles } = state;
  const img = new Image();   // Create new img element
  img.addEventListener('load', function() {
  // execute drawImage statements here
    draw_tile(canvas, img, tiles);
  }, false);

  img.src = '/grass4.png';
  return img;
}

export function initGrowing (state) {
  const tiles = state.tiles;
  const now = window.performance.now();
  for (let i = 0; i < tiles.length; i++) {
    for (let j = 0; j < tiles[i].length; j++) {
      if (tiles[i][j] != 5) {
        const tileKey = createKey(i,j);
        state.growingTiles[tileKey] = { 
          position: [i,j], 
          start: now,
          update: false
        };
      }
    }
  }
}
