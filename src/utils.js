export function createKey(x, y) {
    const key = x.toString().padStart(3, '0') + y.toString().padStart(3, '0');
    return key;
}