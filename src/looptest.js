import { init } from './init';
import { ground } from './ground';

const state = {
  tiles: ground,
  originaltilesize: 80,
  tilesize: 20,
  tilesetNumTiles: 3,
  menu: { menuVisible: false, xPosition: 0, yPosition: 0},
  selectedTiles: {},
  growingTiles: {}
}

const canvas = document.getElementById('cw');
const ctx = canvas.getContext('2d');

window.state = state;

function createKey(x, y) {
  const key = x.toString().padStart(3, '0') + y.toString().padStart(3, '0');
  return key;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function initGrowing () {
  const tiles = state.tiles;
  const now = window.performance.now();
  for (let i = 0; i < tiles.length; i++) {
    for (let j = 0; j < tiles[i].length; j++) {
      if (tiles[i][j] != 5) {
        const tileKey = createKey(i,j);
        state.growingTiles[tileKey] = { 
          position: [i,j], 
          start: now,
          update: false
        };
      }
    }
  }
}

function transitionTileType (currentTileType) {
  let nextTileType = 5;
  if (currentTileType == 4) {
    nextTileType = 8;
  } else if (currentTileType == 6) {
    nextTileType = getRandomInt(0,10) >= 4 ? 0 : 1; 
  } else if (currentTileType == 0 || currentTileType == 1) {
    nextTileType = getRandomInt(0,10) >= 4 ? 2 : 3;
  } else if (currentTileType == 2 || currentTileType == 3) {
    nextTileType = 4;
  }
  return nextTileType;
}

function update (timestamp) {
  //console.log(timestamp);
  //console.log()
  const growingTiles = Object.keys(state.growingTiles);
  for (const tileKey of growingTiles) {
    //drawTile(img, ctx, 6, tile[1], tile[0]);
    const tile = state.growingTiles[tileKey];
    const tileNumRow = tile.position[1];
    const tileNumCol = tile.position[0];
    const currentTileType = state.tiles[tileNumCol][tileNumRow];
    if (currentTileType == 8) {
      delete state.growingTiles[tileKey];
      return;
    }
    if (timestamp - tile.start >= 5000) {
      //console.log(tile.start, timestamp);
      tile.start = timestamp;
      state.tiles[tileNumCol][tileNumRow] = transitionTileType(currentTileType);
      tile.update = true;
    }
  }
}

function drawTile(img, ctx, nextTile, tileNumRow, tileNumCol) {
  const tileRow = (nextTile / state.tilesetNumTiles) | 0; // Bitwise OR operation
  const tileCol = (nextTile % state.tilesetNumTiles) | 0;
  const originalTileSize = state.originaltilesize;
  const tileSize = state.tilesize;
  //console.log(tileRow, tileCol);
  ctx.drawImage(img, (tileCol * originalTileSize), (tileRow * originalTileSize), originalTileSize, originalTileSize, (tileNumRow * tileSize), (tileNumCol * tileSize), tileSize, tileSize);
  state.tiles[tileNumCol][tileNumRow] = nextTile;
}

function render () {
  const growingTiles = Object.values(state.growingTiles);
  for (const tile of growingTiles) {
    if (tile.update) {
      const tileNumRow = tile.position[1]
      const tileNumCol = tile.position[0]
      const tileToDraw = state.tiles[tileNumCol][tileNumRow];
      tile.update = false;
      drawTile(img, ctx, tileToDraw, tileNumRow, tileNumCol);
    }
  }
}

// The main game loop
function main (timestamp) {
  // run the update function
  update(timestamp);
  // run the render function
  render();
  // Request to do this again ASAP
  window.requestAnimationFrame(main);
}

// Let's play this game!
const img = init(canvas, state);
initGrowing();
main();



