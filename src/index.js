import { init, initGrowing } from './init';
import { ground } from './ground';
import { update, render } from './system';

const state = {
  tiles: ground,
  originaltilesize: 80,
  tilesize: 20,
  tilesetNumTiles: 3,
  menu: { menuVisible: false, xPosition: 0, yPosition: 0 },
  selectedTiles: {},
  growingTiles: {}
}

const canvas = document.getElementById('cw');
const ctx = canvas.getContext('2d');

// The main game loop
function main (timestamp) {
    // run the update function
    update(state, timestamp);
    // run the render function
    render(img, ctx, state);
    // Request to do this again ASAP
    window.requestAnimationFrame(main);
  }
  
  // Let's play this game!
  const img = init(canvas, state);
  initGrowing(state);
  main();