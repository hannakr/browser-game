window.onload = function() {
  console.log('Hi!');
}

function draw_island(json) {
  const canvas = document.getElementById('cw');
  if (!canvas.getContext) {
    return;
  }
  const ctx = canvas.getContext('2d');
  ctx.fillStyle = '#5D4037';

  const h = 5;
  const w = 5;

  for(const key in json) {
    const cell = json[key];
    ctx.fillRect(cell['x'], cell['y'], w, h);
  }
}

document.getElementById('import').onclick = function() {
  const files = document.getElementById('selectFiles').files;
  console.log(files);
  if (files.length <= 0) {
    return false;
  }

  const fr = new FileReader();

  fr.onload = function(e) {
    console.log(e);
    const result = JSON.parse(e.target.result);
    const formatted = JSON.stringify(result, null, 2);
    //document.getElementById('result').value = formatted;

    draw_island(result);
  }

  fr.readAsText(files.item(0));
};
