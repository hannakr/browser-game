function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function transitionTileType(currentTileType) {
  let nextTileType = 5;
  if (currentTileType == 4) {
    nextTileType = 8;
  } else if (currentTileType == 6) {
    nextTileType = getRandomInt(0, 10) >= 4 ? 0 : 1;
  } else if (currentTileType == 0 || currentTileType == 1) {
    nextTileType = getRandomInt(0, 10) >= 4 ? 2 : 3;
  } else if (currentTileType == 2 || currentTileType == 3) {
    nextTileType = 4;
  }
  return nextTileType;
}

export function update(state, timestamp) {
  //console.log(timestamp);
  //console.log()
  const growingTiles = Object.keys(state.growingTiles);
  for (const tileKey of growingTiles) {
    //drawTile(img, ctx, 6, tile[1], tile[0]);
    const tile = state.growingTiles[tileKey];
    const tileNumRow = tile.position[1];
    const tileNumCol = tile.position[0];
    const currentTileType = state.tiles[tileNumCol][tileNumRow];
    if (currentTileType == 8) {
      delete state.growingTiles[tileKey];
      return;
    }
    if (timestamp - tile.start >= 5000) {
      //console.log(tile.start, timestamp);
      tile.start = timestamp;
      state.tiles[tileNumCol][tileNumRow] = transitionTileType(currentTileType);
      tile.update = true;
    }
  }
}

function drawTile(img, ctx, state, nextTile, tileNumRow, tileNumCol) {
  const tileRow = (nextTile / state.tilesetNumTiles) | 0; // Bitwise OR operation
  const tileCol = nextTile % state.tilesetNumTiles | 0;
  const originalTileSize = state.originaltilesize;
  const tileSize = state.tilesize;
  //console.log(tileRow, tileCol);
  ctx.drawImage(
    img,
    tileCol * originalTileSize,
    tileRow * originalTileSize,
    originalTileSize,
    originalTileSize,
    tileNumRow * tileSize,
    tileNumCol * tileSize,
    tileSize,
    tileSize,
  );
  state.tiles[tileNumCol][tileNumRow] = nextTile;
}

export function render(img, ctx, state) {
  const growingTiles = Object.values(state.growingTiles);
  for (const tile of growingTiles) {
    if (tile.update) {
      const tileNumRow = tile.position[1];
      const tileNumCol = tile.position[0];
      const tileToDraw = state.tiles[tileNumCol][tileNumRow];
      tile.update = false;
      drawTile(img, ctx, state, tileToDraw, tileNumRow, tileNumCol);
    }
  }
}
