function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function createKey(x, y) {
  const key = x.toString().padStart(3, '0') + y.toString().padStart(3, '0');
  return key;
}

function getNeighbours(x, y, w, h) {
  const n1 = [x, y-h];
  const n2 = [x+w, y];
  const n3 = [x, y+h];
  const n4 = [x-w, y];
  return [n1,n2,n3,n4];
}

function getRandomPoint(rect) {
  const x = getRandomInt(rect.x, rect.x+rect.w);
  const y = getRandomInt(rect.y, rect.y+rect.h);
  return [x,y];
}

function drawline(x0,y0,x1,y1){
  var tmp;
  var steep = Math.abs(y1-y0) > Math.abs(x1-x0);
  if(steep){
    //swap x0,y0
    tmp=x0; x0=y0; y0=tmp;

    //swap x1,y1
    tmp=x1; x1=y1; y1=tmp;
  }

  var sign = 1;
  if(x0>x1){
    sign = -1;
    x0 *= -1;
    x1 *= -1;
  }
  var dx = x1-x0;
  var dy = Math.abs(y1-y0);
  var err = ((dx/2));
  var ystep = y0 < y1 ? 10:-10;
  var y = y0;

  const points = [];
  for(var x=x0;x<=x1;x+=10){
    if(!(steep ? points.push([y,sign*x]) : points.push([sign*x,y]))) return;
    err = (err - dy);
    if(err < 0){
      y+=ystep;
      err+=dx;
    }
  }
  return points;
}

function draw_room() {
  const canvas = document.getElementById('cw');
  if (!canvas.getContext) {
    return;
  }
  const ctx = canvas.getContext('2d');
  ctx.fillStyle = '#5D4037';

  const start = {'x': 128, 'y': 140, 'w': 133, 'h': 98};
  const h1 = 40;
  const w2 = 80;
  const h3 = 23;
  const w4 = 110;
  const rect1 = {'x': start.x, 'y': start.y-h1, 'w': start.w, 'h': h1};
  const rect2 = {'x': start.x+start.w, 'y': start.y, 'w': w2, 'h': start.h};
  const rect3 = {'x': start.x, 'y': start.y+start.h, 'w': start.w, 'h': h3};
  const rect4 = {'x': start.x-w4, 'y': start.y, 'w': w4, 'h': start.h};

  ctx.fillRect(start.x, start.y, start.w, start.h);
  ctx.strokeRect(rect1.x, rect1.y, rect1.w, rect1.h);
  ctx.strokeRect(rect2.x, rect2.y, rect2.w, rect2.h);
  ctx.strokeRect(rect3.x, rect3.y, rect3.w, rect3.h);
  ctx.strokeRect(rect4.x, rect4.y, rect4.w, rect4.h);

  const point1 = getRandomPoint(rect1);
  console.log(point1);
  const point2 = getRandomPoint(rect2);
  console.log(point2);
  const point3 = getRandomPoint(rect3);
  const point4 = getRandomPoint(rect4);

  ctx.fillRect(point1[0], point1[1], 2, 2);
  ctx.fillRect(point2[0], point2[1], 2, 2);
  ctx.fillRect(point3[0], point3[1], 2, 2);
  ctx.fillRect(point4[0], point4[1], 2, 2);

  const points1 = drawline(point1[0], point1[1], point2[0], point2[1]);
  const points2 = drawline(point2[0], point2[1], point3[0], point3[1]);
  const points3 = drawline(point3[0], point3[1], point4[0], point4[1]);
  const points4 = drawline(point4[0], point4[1], point1[0], point1[1]);
  //console.log(points1);

  ctx.fillStyle = 'orange';
  points1.forEach(function(point) {
    ctx.fillRect(point[0], point[1], 10, 10);
  });
  points2.forEach(function(point) {
    ctx.fillRect(point[0], point[1], 10, 10);
  });
  points3.forEach(function(point) {
    ctx.fillRect(point[0], point[1], 10, 10);
  });
  points4.forEach(function(point) {
    ctx.fillRect(point[0], point[1], 10, 10);
  });
}

function draw_sir() {
  const canvas = document.getElementById('cw');
  if (!canvas.getContext) {
    return;
  }
  const ctx = canvas.getContext('2d');
  ctx.fillStyle = '#5D4037';

  const coordinates = {};
  const start = [250, 250];
  coordinates[createKey(start[0],start[1])] = { 'x': start[0], 'y': start[1], 'checked': 0};
  const cells = [start];
  const h = 3;
  const w = 3;
  for (let i = 0; i < 120; i++) {
    // neighbours if start point is x,y; square height is h; width is w:
    // (x, y-h)
    // (x+w, y)
    // (x, y+h)
    // (x-w, y)
    for(const key in coordinates) {
      const cell = coordinates[key];
      if (cell['checked'] <= 1) {
        const neighbours = getNeighbours(cell['x'], cell['y'], w, h);
        for (let j = 0; j < 4; j++) {
          const nkey = createKey(neighbours[j][0], neighbours[j][1]);
          if (!(nkey in coordinates)) {
            if (getRandomInt(0,10) >= 7) {
              coordinates[nkey] = { 'x': neighbours[j][0], 'y': neighbours[j][1], 'checked': 0};
            }
          }
        }
        cell['checked']++;
      }
    }
    //console.log(Object.keys(coordinates).length);
  }
  for(const key in coordinates) {
    const cell = coordinates[key];
    ctx.fillRect(cell['x'], cell['y'], w, h);
  }
  buildJSON(coordinates);
}

function buildJSON(obj) {
  const jsonstr = JSON.stringify(obj);
  const dataURL = `data:application/json,${jsonstr}`;

  const anchor = document.querySelector("a");
  anchor.setAttribute("download", "island.json");
  anchor.setAttribute("href", dataURL);
}

window.onload = function() {
  // const cn = document.getElementById('cw');
  // const ctx = cn.getContext('2d');

  draw_sir();
  //draw_room();
}
