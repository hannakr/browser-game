import { init } from './init';
import { ground } from './ground';

const state = {
  tiles: ground,
  originaltilesize: 80,
  tilesize: 20,
  tilesetNumTiles: 3,
  menu: { menuVisible: false, xPosition: 0, yPosition: 0},
  selectedTiles: {}
}

window.state = state;

function createKey(x, y) {
  const key = x.toString().padStart(3, '0') + y.toString().padStart(3, '0');
  return key;
}

function placeMenu (tileNumRow, tileNumCol, tileSize) {
  let menuX = 0;
  let menuY = 0;
  const tileX = tileNumRow * tileSize;
  const tileY = tileNumCol * tileSize;
  const xLimit = tileSize + 85;
  const yLimit = 500 - 45;
  if (tileX >= xLimit) {
    menuX = tileX - tileSize - 85;
  } else {
    menuX = tileX + (2 * tileSize);
  }
  if (tileY <= yLimit) {
    menuY = tileY;
  } else {
    menuY = tileY + tileSize - 40;
  }
  return [menuX,menuY];
}

function updateMenuState (menuX, menuY) {
  state.menu.xPosition = menuX;
  state.menu.yPosition = menuY;
}

function isClickInMenu (mouseX, mouseY) {
  //console.log('menu compare');
  //console.log(mouseX, state.menu.xPosition);
  //console.log(mouseY, state.menu.yPosition);
  if (mouseX < state.menu.xPosition + 80 && mouseX > state.menu.xPosition) {
    if (mouseY > state.menu.yPosition && mouseY < state.menu.yPosition + 40) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function menuSelection (mouseX, mouseY) {
  const menuX = state.menu.xPosition;
  const menuY = state.menu.yPosition;
  const gutterX = 12;
  const gutterY = 8;
  const width = 24;
  const height = 24;
  //console.log('selection');
  //console.log(mouseX, state.menu.xPosition);
  //console.log(mouseY, state.menu.yPosition);
  if (mouseX < (menuX + gutterX + width) && mouseX > (menuX + gutterX)) {
    //console.log('in selectionX');
    if (mouseY < (menuY + gutterY + height) && mouseY > (menuY + gutterY)) {
      //console.log('in selectionY');
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function drawMenu (img, ctxMenu, tileNumRow, tileNumCol, tileSize) {
  const originalTileSize = state.originaltilesize;
  if (state.menu.menuVisible) {
    clearMenu(ctxMenu);
  }
  const menuCoords = placeMenu(tileNumRow, tileNumCol, tileSize);
  ctxMenu.drawImage(img, 0 * originalTileSize, 4 * originalTileSize, 2 * originalTileSize, originalTileSize, menuCoords[0], menuCoords[1], 80, 40);
  state.menu.menuVisible = true;
  updateMenuState(menuCoords[0], menuCoords[1]);
}

function clearMenu (ctxMenu) {
  ctxMenu.clearRect(0,0,500,500);
}

function drawTile(img, ctx, nextTile, tileNumRow, tileNumCol) {
  const tileRow = (nextTile / state.tilesetNumTiles) | 0; // Bitwise OR operation
  const tileCol = (nextTile % state.tilesetNumTiles) | 0;
  const originalTileSize = state.originaltilesize;
  const tileSize = state.tilesize;
  //console.log(tileRow, tileCol);
  ctx.drawImage(img, (tileCol * originalTileSize), (tileRow * originalTileSize), originalTileSize, originalTileSize, (tileNumRow * tileSize), (tileNumCol * tileSize), tileSize, tileSize);
  state.tiles[tileNumCol][tileNumRow] = nextTile;
}

function menuClicked (mouseX, mouseY, ctxMenu, ctx, img) {
  if (menuSelection(mouseX, mouseY)) {
    clearMenu(ctxMenu);
    const selectedTiles = Object.values(state.selectedTiles);
    for (const tile of selectedTiles) {
      drawTile(img, ctx, 6, tile[1], tile[0]);
    }
    state.selectedTiles = {};
  }
}

function tileClicked (img, ctx, ctxMenu, mouseX, mouseY) {
  const tileSize = state.tilesize;
  const tileNumRow = (Math.floor(mouseX/tileSize));
  const tileNumCol = (Math.floor(mouseY/tileSize));

  console.log(tileNumRow, tileNumCol);
  const tileKey = createKey(tileNumCol,tileNumRow);
  const tileType = state.tiles[tileNumCol][tileNumRow];
  if (tileType == 5) {
    drawTile(img, ctx, 9, tileNumRow, tileNumCol);
    state.selectedTiles[tileKey] = [tileNumCol,tileNumRow];
    drawMenu(img, ctxMenu, tileNumRow, tileNumCol, tileSize);
  } else if (tileType == 9) {
    drawTile(img, ctx, 5, tileNumRow, tileNumCol);
    delete state.selectedTiles[tileKey];
    if (Object.keys(state.selectedTiles).length == 0) {
      state.menu.menuVisible = false;
      clearMenu(ctxMenu);
    }
  }
}

function getClickHandler (canvas, menuCanvas, img, state) {
  const ctx = canvas.getContext('2d');
  const ctxMenu = menuCanvas.getContext('2d');
  const tileSize = state.tilesize;
  const canvasWidth = 500;
  const canvasHeight = 500;

  return function handleClick(e) {
    //console.log(e.clientX, e.clientY);
    const mouseX = e.clientX;
    const mouseY = e.clientY;
    if (state.menu.menuVisible) {
      if (isClickInMenu(mouseX, mouseY)) {
      //menuClicked Event
        menuClicked(mouseX, mouseY, ctxMenu, ctx, img);
      } else {
        //tileClicked Event
        tileClicked(img, ctx, ctxMenu, mouseX, mouseY);
      }
    } else {
      //tileClicked Event
      tileClicked(img, ctx, ctxMenu, mouseX, mouseY);
    }
  }
}

window.onload = function() {
  const canvas = document.getElementById('cw');
  const menuCanvas = document.getElementById('cw2');
  const img = init(canvas, state);
  const handleClick = getClickHandler(canvas, menuCanvas, img, state);

  document.addEventListener('click', handleClick);
}
